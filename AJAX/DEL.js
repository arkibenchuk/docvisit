const BASE_URL_CARDS = 'https://ajax.test-danit.com/api/v2/cards'
const TOKEN = '5bf09201-56fa-4731-9bc3-1c1bdce34a57'

async function deleteCards(id) {
	await fetch(`${BASE_URL_CARDS}/${id}`, {
		method: 'DELETE',
		headers: {
			Authorization: `Bearer ${TOKEN}`,
		},
	})
	console.log('DEL')
}
export default deleteCards
