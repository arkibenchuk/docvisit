const BASE_URL_CARDS = 'https://ajax.test-danit.com/api/v2/cards'
const TOKEN = '5bf09201-56fa-4731-9bc3-1c1bdce34a57'

async function getCards() {
	let responseURL = await fetch(BASE_URL_CARDS, {
		method: 'GET',
		headers: {
			Authorization: `Bearer ${TOKEN}`,
		},
	})

	if (!responseURL.ok) {
		throw new Error(
			`Ошибка по адресу ${BASE_URL_CARDS}, статус ошибки ${responseURL.status}`
		)
	}
	let resultFetch = await responseURL.json()

	return resultFetch
}
export default getCards
