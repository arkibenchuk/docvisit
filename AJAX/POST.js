const BASE_URL_CARDS = 'https://ajax.test-danit.com/api/v2/cards'
const TOKEN = '5bf09201-56fa-4731-9bc3-1c1bdce34a57'


async function SendCard(obj) {
	let ResponseURL = await fetch(`${BASE_URL_CARDS}`, {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json;',
			Authorization: `Bearer ${TOKEN}`,
		},
		body: obj,
	})

	if (!ResponseURL.ok) {
		throw new Error(
			`Ошибка по адресу ${BASE_URL_CARDS}, статус ошибки ${ResponseURL.status}`
		)
	}

	return ResponseURL.json()
}
export default SendCard
