const BASE_URL_CARDS = 'https://ajax.test-danit.com/api/v2/cards'
const TOKEN = '5bf09201-56fa-4731-9bc3-1c1bdce34a57'

async function ChengeCards(id, obj) {
	let ResponseURL = await fetch(`${BASE_URL_CARDS}/${id}`, {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json;',
			Authorization: `Bearer ${TOKEN}`,
		},
		body: obj,
	})
	console.log('PUT')
	if (!ResponseURL.ok) {
		throw new Error(
			`Ошибка по адресу ${BASE_URL_CARDS}, статус ошибки ${ResponseURL.status}`
		)
	}
	return await ResponseURL.json()
}
export default ChengeCards
