// import editCard from './editCard'

let DoctorChoise = (event, elementID) => {
	try {
		event.preventDefault()

		let container = document.querySelector('.edit-form')
		let formVisit = container.querySelector('.visit-dynamic-box')
		let doctorSelect = container.querySelector('.doctor-select')
		let form = document.querySelector('.edit-form')

		doctorSelect.addEventListener('click', function (event) {
			let array = JSON.parse(localStorage.getItem(`${elementID}`))

			let target = event.target
			switch (target.value) {
				case 'Dentist':
					formVisit.innerHTML = ``
					formVisit.insertAdjacentHTML(
						'beforeend',
						`    <div class="visit-select-wrap">
    			<div><span class="select-title">Last visit date</span></div>
    				 <div><input class="last_Visit" name="last_Visit" type="text" /></div>
    					</div>`
					)
					let last_Visit = form.querySelector('.last_Visit')
					if (array.last_Visit !== undefined) {
						last_Visit.value = `${array.last_Visit}`
					} else {
						last_Visit.value = ` `
					}

					break
				case 'Cardiologist':
					formVisit.innerHTML = ``
					formVisit.insertAdjacentHTML(
						'beforeend',
						`    <div class="visit-select-wrap">
			<div><span class="select-title">Blood pressure</span></div>
			   <div><input name="Blood_Pressure" class="Blood_Pressure" type="text"/></div>
    			</div>
     			<div class="visit-select-wrap"> 			<div><span class="select-title ">Mass index</span></div>
		   <div><input class="Mass_Index" name="Mass_Index" type="text"/></div>
    			</div>
    		<div class="visit-select-wrap">
    		<div><span class="select-title">Heart illnesses</span></div>
	  		 <div><input class="Heart_illnesses" name="Heart_illnesses" type="text"/></div>
    			</div>
 			<div class="visit-select-wrap">
    			<div><span class="select-title">Age</span></div>
    		   <div><input  class="Age" name="Age" type="text"/></div>
     			</div>
     			`
					)
					let Blood_Pressure = form.querySelector('.Blood_Pressure')
					let Mass_Index = form.querySelector('.Mass_Index')
					let Heart_illnesses = form.querySelector('.Heart_illnesses')
					let Age = form.querySelector('.Age')

					if (array.Blood_Pressure !== undefined) {
						Blood_Pressure.value = `${array.Blood_Pressure}`
					} else {
						Blood_Pressure.value = ` `
					}
					if (array.Mass_Index !== undefined) {
						Mass_Index.value = `${array.Mass_Index}`
					} else {
						Mass_Index.value = ` `
					}
					if (array.Heart_illnesses !== undefined) {
						Heart_illnesses.value = `${array.Heart_illnesses}`
					} else {
						Heart_illnesses.value = ` `
					}
					if (array.Age !== undefined) {
						Age.value = `${array.Age}`
					} else {
						Age.value = ` `
					}
					break
				case 'Therapist':
					formVisit.innerHTML = ``
					formVisit.insertAdjacentHTML(
						'beforeend',
						`    <div class="visit-select-wrap">
    					<div><span class="select-title">Age</span></div>
    				 	<div><input class="TherapistAge" name="Age" type="text"/></div>
    					</div>`
					)
					let TherapistAge = form.querySelector('.TherapistAge')

					if (array.TherapistAge !== undefined) {
						TherapistAge.value = `${array.Age}`
					} else {
						TherapistAge.value = ` `
					}
					break
			}
		})
	} catch (error) {}
}
export default DoctorChoise
