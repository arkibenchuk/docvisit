import DoctorChoise from './DoctorChoise.js'
import * as modal from './modal.js'

let EditRender = (id) => {
	let array = JSON.parse(localStorage.getItem(`${id}`))


	let container = document.querySelector('.edit-container')

	try {
		let EditForm = `
       
          <form class="modal-form edit-form">
          <h4 class="modal-title">Edit visit</h4>
          <div class="visit-form-wrap">
          <div class="visit-select-wrap">
          <h5 class="select-title">Choose doctor</h5>
      
          <select name="Doc" class="doctor-select">
              <option value="">Select Doc…</option>
              <option value="Dentist">Dentist</option>
              <option value="Cardiologist">Cardiologist</option>
             <option value="Therapist">Therapist</option>
         </select>
        </div>
            <div class="visit-select-wrap">
                <div><span class="select-title">Visit purpose</span></div>
                <div>
                    <input class="purpose" name="purpose" type="text"/>
              </div>
            </div>
            <div class="visit-select-wrap">
              <div><span class="select-title">Brief description</span></div>
              <div>
                  <input  class="Brief_Description"  name="Brief_Description" type="text"/>
            </div>
          </div>
          <div class="visit-select-wrap">
              <div><span class="select-title">Your name</span></div>
             <div><input class="full_name" name="full_name" type="text"/> </input> </div>
          </div>
          <div class="visit-select-wrap">
              <div><span class="select-title">Urgency</span></div>
              <select name="urgency">
                  <option value="All">All</option>
                  <option value="High">High</option>
                  <option value="Normal">Normal</option>
                  <option value="Low">Low</option>
              </select>  
          </div>
          <div class="visit-dynamic-box">
          </div>
          <div class="btn-wrapper">
              <button type="submit" id="1" class="btn edit-submit">Edit</button>
          </div>
  
          <div class="modal-close">
              <img src="img/cancel.svg" class="close-edit"  alt="" width="30px">
          </div>     
      </form>
    
      `
		container.innerHTML = EditForm

		let form = document.querySelector('.edit-form')
		let full_name = form.querySelector('.full_name')
		let purpose = form.querySelector('.purpose')
		let Brief_Description = form.querySelector('.Brief_Description')
		let urgency = form.querySelector('.urgency')
		try {
			if (array.full_name !== undefined) {
				full_name.value = `${array.full_name}`
			} else {
				full_name.value = ` `
			}

			if (array.purpose !== undefined) {
				purpose.value = `${array.purpose}`
			} else {
				purpose.value = ` `
			}

			if (array.Brief_Description !== undefined) {
				Brief_Description.value = `${array.Brief_Description}`
			} else {
				Brief_Description.value = ` `
			}

			if (array.urgency !== undefined) {
				urgency.value = `${array.urgency}`
			} else {
				urgency.value = ` `
			}
		} catch {}

		// let closeEditCard = form.querySelector('.close-edit')
		// console.log(closeEditCard)
		// closeEditCard.addEventListener('click', () => {
		// 	container.removeChild(form)
		// })
		console.log(array.purpose)
		console.log(purpose)
	} catch (e) {
		console.error(e)
	}
}
export default EditRender
