import * as modal from './modal.js'
import getAllCards from './getAllCards.js'
import Exit from './Exit.js'

let LocalAuthorization = async () => {
	document.addEventListener('DOMContentLoaded', (e) => {
		e.preventDefault()
		if (localStorage.getItem('UserExsist')) {
			getAllCards()
			modal.visitModal.visitBtnShow()
			Exit()
		}
	})
}
export default LocalAuthorization
