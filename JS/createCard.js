import renderCard from './renderCard.js'
import * as modal from './modal.js'
import SendCard from '../AJAX/POST.js'

let SendCards = (event) => {
	let cardForm = document.querySelector('.visit-form')
	const btnSubmit = document.querySelector('.btn-submit')
	// DoctorChoise(event)

	btnSubmit.addEventListener('click', async (e) => {
		e.preventDefault()
		let formData = new FormData(cardForm)
		let obj_Created = {}
		for (let [key, value] of formData) {
			obj_Created[key] = value
		}
		let obj_Sended = JSON.stringify(obj_Created)
		let responseServer = await SendCard(obj_Sended)
		renderCard(responseServer)
		localStorage.setItem(responseServer.id, JSON.stringify(responseServer))
		modal.visitModal.close()
		let elementContainer = document.querySelector('.cards-container')
		let noCardElem = document.getElementById('empty')
		if (noCardElem) {
			elementContainer.removeChild(noCardElem)
		}
		cardForm.reset()
	})
}

export default SendCards
