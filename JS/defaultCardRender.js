async function noCardsRender() {
	try {
		let EmptyCard = `
        <div class="glass-card" id="empty">
        <h5 class="default-title">No items have been added</h5>
     </div>
		`
		let container = document.querySelector('.cards-container')
		container.insertAdjacentHTML('afterbegin', EmptyCard)
	} catch (e) {
		console.error(e)
	}
}
export default noCardsRender
