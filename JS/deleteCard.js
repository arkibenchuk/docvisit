import noCardsRender from './defaultCardRender.js'
import deleteCards from '../AJAX/DEL.js'

let delateCard = async (event) => {
	event.preventDefault()
	let elementContainer = document.querySelector('.cards-container')
	let elementID = event.target.closest('.glass-card').id
	await deleteCards(elementID)

	let element = document.getElementById(`${elementID}`)
	elementContainer.removeChild(element)
	if (!elementContainer.querySelector('.glass-card')) {
		noCardsRender()
	}

	localStorage.removeItem(`${elementID}`)
}

export default delateCard
