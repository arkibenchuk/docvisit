import * as modal from './modal.js'
import DoctorChoise from './DoctorChoise.js'
import ChengeCards from '../AJAX/PUT.js'

let editCard = (elementID) => {
	// event.preventDefault()
	let cardForm = document.querySelector('.edit-form')
	let editSubmit = document.getElementById('1')
	// modal.editVisitModal.show()
	// DoctorChoise()
	editSubmit.addEventListener('click', async (event) => {
		event.preventDefault()
		let formData = new FormData(cardForm)
		let obj_Created = {}
		for (let [key, value] of formData) {
			obj_Created[key] = value
		}
		let obj_Sended = JSON.stringify(obj_Created)
		let responseServer = await ChengeCards(elementID, obj_Sended)
		localStorage.setItem(responseServer.id, JSON.stringify(responseServer))
		///////////////////////////////////////////////////

		let card = document.getElementById(elementID)
		let full_nameEdit = card.querySelector('.card-text')
		let purposeEdit = card.querySelector('.purpose-title')
		let urgencyEdit = card.querySelector('.urgency')
		// let Blood_PressureEdit = card.querySelector('.card-title')
		// let Mass_IndexEdit = card.querySelector('.card-title')
		// let Heart_illnessesEdit = card.querySelector('.card-title')
		// let AgeEdit = card.querySelector('.card-title')
		let Brief_DescriptionEdit = card.querySelector('.Brief_Description-t')
		let last_VisitEdit = card.querySelector('.last_Visit')

		let {
			full_name,
			purpose,
			Brief_Description,
			urgency,
			Blood_Pressure,
			Mass_Index,
			Heart_illnesses,
			Age,
			last_Visit,
		} = responseServer
		console.log(purpose)
		try {
			full_nameEdit.innerHTML = `${full_name}`
			purposeEdit.innerHTML = `${purpose}`
			urgencyEdit.innerHTML = `${urgency}`
			Blood_PressureEdit.innerHTML = `${Blood_Pressure}`
			Mass_IndexEdit.innerHTML = `${Mass_Index}`
			Heart_illnessesEdit.innerHTML = `${Heart_illnesses}`
			AgeEdit.innerHTML = `${Age}`
			// Brief_DescriptionEdit.innerHTML = `${Brief_Description}`
			// last_VisitEdit.innerHTML = `${last_Visit}`
		} catch (error) {}

		//////////////////////////////////////////////////
		// modal.editVisitModal.close()
	})
	// cardForm.reset()
}

export default editCard
