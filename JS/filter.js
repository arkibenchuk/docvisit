let filter = () => {
	let cardContainer = document.querySelector('.cards-container')
	let filterElem = document.getElementById('filter')
	
	filterElem.onchange = function () {

		let items = cardContainer.getElementsByClassName('glass-card')
		for (let i = 0; i < items.length; i++) {
			if (items[i].classList.contains(this.value)) {
				items[i].style.display = 'flex'
			} else if (this.value == 'All') {
				items[i].style.display = 'flex'
			} else {
				items[i].style.display = 'none'
			}
		}
	}
}
export default filter
