import renderCard from './renderCard.js'
import getCards from '../AJAX/GET.js'
import noCardsRender from './defaultCardRender.js'

let getAllCards = async () => {
	let servCardArr = await getCards()
	servCardArr.forEach((element) => {
		renderCard(element)
		localStorage.setItem(element.id, JSON.stringify(element))
	})
	if (servCardArr.length == 0) {
		noCardsRender()
	}
}
export default getAllCards
