import SendCards from './createCard.js'
import LocalAuthorization from './Local.js'
import Authorization from './authorization.js'
import * as modal from './modal.js'
import editCard from './editCard.js'
import delateCard from './deleteCard.js'
import EditRender from './EditRender.js'
// import showMore from './showMore.js'
import DoctorChoise from './DoctorChoise.js'
import filter from './filter.js'
import show from './show.js'

let btnEnter = document.querySelector('.btn-enter')

modal.visitModal.visitBtnHide()
// localStorage.clear()
LocalAuthorization()

btnEnter.addEventListener('click', async (e) => {
	e.preventDefault()
	Authorization()
})

SendCards()

document.body.addEventListener('click', (event) => {
	if (event.target.classList.contains('fiter-card')) {
		filter()
	}
})

document.body.addEventListener('click', async function (event) {
	if (event.target.classList.contains('edit-card')) {
		let elementID = event.target.closest('.glass-card').id
		modal.editModal.show()
		EditRender(elementID)
		editCard(elementID)
		DoctorChoise(event, elementID)
		modal.editModal.editModalClose()
	}
})

document.body.addEventListener('click', function (event) {
	if (event.target.classList.contains('close-card')) {
		delateCard(event)
	}
})

// document.body.addEventListener('click', function (event) {
// 	if (event.target.classList.contains('btn-show-more')) {
// 		showMore(event)
// 	}
// })

document.body.addEventListener('click', function (event) {
	if (event.target.classList.contains('search-btn')) {
		let searchInput = document.querySelector('.search').value
		const cardsList = document.getElementsByClassName('glass-card')

		for (let i = 0; i < cardsList.length; i++) {
			if (!cardsList[i].innerText.toLowerCase().includes(searchInput)) {
				cardsList[i].style.display = 'none'
			} else {
				cardsList[i].style.display = 'flex'
			}
		}
	}
})

const doctorSelect = document.querySelector('.doctor-select')
const formVisit = document.querySelector('.visit-dynamic-box')
doctorSelect.addEventListener('click', function (event) {
	const target = event.target
	switch (target.value) {
		case 'Dentist':
			formVisit.innerHTML = ``
			formVisit.insertAdjacentHTML(
				'beforeend',
				`    <div class="visit-select-wrap">
			<div><span class="select-title">Last visit date</span></div>
				 <div><input name="last_Visit" type="text" /></div>
					</div>`
			)
			break
		case 'Cardiologist':
			formVisit.innerHTML = ``
			formVisit.insertAdjacentHTML(
				'beforeend',
				`    <div class="visit-select-wrap">
				<div><span class="select-title">Blood pressure</span></div>
			   <div><input name="Blood_Pressure" type="text"/></div>
			</div>
			<div class="visit-select-wrap">
			<div><span class="select-title ">Mass index</span></div>
		   <div><input name="Mass_Index" type="text"/></div>
			</div>
			<div class="visit-select-wrap">
			<div><span class="select-title">Heart illnesses</span></div>
	  		 <div><input name="Heart_illnesses" type="text"/></div>
			</div>
			<div class="visit-select-wrap">
			<div><span class="select-title">Age</span></div>
		   <div><input name="Age" type="text"/></div>
			</div>
			`
			)
			break
		case 'Therapist':
			formVisit.innerHTML = ``
			formVisit.insertAdjacentHTML(
				'beforeend',
				`    <div class="visit-select-wrap">
					<div><span class="select-title">Age</span></div>
				 	<div><input name="Age" type="text"/></div>
					</div>`
			)
			break
	}
})

document.body.addEventListener('click', function (event) {
	if (event.target.classList.contains('btn-show-more')) {
		modal.showMoreModal.showMoreOpen(event)
		show(event)
		// modal.showMoreModal.close()
		modal.showMoreModal.showModalClose(event)
	}
})

document.addEventListener('DOMContentLoaded', (e) => {
	e.preventDefault()
	let colors = ['Orange_Fun', 'Argon', 'Visions_of_Grandeur', 'moonlight','kye']
	let randomIndex = Math.floor(Math.random() * colors.length)
	let bodyDocument = document.querySelector('.body')
	console.log(bodyDocument)
	bodyDocument.classList.add(colors[randomIndex])
})
