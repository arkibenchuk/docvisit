
const loginBtn = document.querySelector('.btn-login')
const visitBtn = document.querySelector('.btn-create-visit')
const closebtnLogin = document.querySelector('.close-login')
const closeBtnVisit = document.querySelector('.close-visit')

class Modal {
	constructor(selector) {
		this.$el = document.querySelector(selector)
	}

	show() {
		this.$el.style.opacity = '1'
		this.$el.style.visibility = 'visible'
	}

	

	close() {
		this.$el.style.opacity = '0'
		this.$el.style.visibility = 'hidden'
	}


	inputError() {
		const emailInput = document.getElementById('emailValue')
		const passInput = document.getElementById('passValue')

		emailInput.style.border = '2px solid red'
		passInput.style.border = '2px solid red'
		emailInput.value = ''
		passInput.value = ''

		emailInput.addEventListener('focus', function () {
			emailInput.style.border = '2px solid transparent'
			passInput.style.border = '2px solid transparent'
		})
		passInput.addEventListener('focus', function () {
			emailInput.style.border = '2px solid transparent'
			passInput.style.border = '2px solid transparent'
		})
	}

	visitBtnHide() {
		visitBtn.style.opacity = '0'
		visitBtn.innerText = ``
	}

	visitBtnShow() {
		visitBtn.style.opacity = '1'
		visitBtn.innerText = `Create visit`
	}


	editModalClose(){
		const closeBtnEdit = document.querySelector('.close-edit')
		closeBtnEdit.addEventListener('click', () => {
				editModal.close()
		})

		const editSubmit = document.querySelector('.edit-submit')
		editSubmit.addEventListener('click', () => {
			editModal.close()
	})

	}

	showModalClose(event){
		const showMoreCloseBtn = document.querySelector('.show-more-cross')


		document.body.addEventListener('click', (event)=>{
			if(event.target.classList.contains('show-more-cross')){
				showMoreModal.delete()
				console.log('123')
			}
			
		})
	}


	delete(){
		const showContainer = document.querySelector('.show-more-modal')
		const showMoreChild = document.querySelector('.show-more-card')
		showContainer.removeChild(showMoreChild)
		showContainer.classList.remove('active-show')

	}

	showMoreOpen(event){
		let showMoreModal = document.querySelector('.show-more-modal')
		showMoreModal.classList.add('active-show')

	}

}


export const loginModal = new Modal('.login-modal')
export const visitModal = new Modal('.visit-modal')
export const editModal = new Modal('.edit-container')
export const showMoreModal = new Modal('.show-more-modal')



loginBtn.addEventListener('click', () => {
	loginModal.show()
})
visitBtn.addEventListener('click', () => {
	visitModal.show()
})
closebtnLogin.addEventListener('click', () => {
	loginModal.close()
})
closeBtnVisit.addEventListener('click', () => {
	visitModal.close()
})
