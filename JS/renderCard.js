async function renderCard(element) {
	let {
		full_name,
		purpose,
		Brief_Description,
		urgency,
		Blood_Pressure,
		Mass_Index,
		Heart_illnesses,
		Age,
		last_Visit,
		id,
	} = element
	try {
		let VistCard = `
	<div class="glass-card ${urgency}" id = "${id}" >
		<div class="card-wrap">
	 			<h5  class="card-title purpose-title">${purpose}</h5>
			<div class="card-body">
				<p class="card-text">${full_name}</p>
				<p class="card-text urgency">${urgency}</p>
			<button class="btn btn-show-more" id="showMore">Show more...</button>
		</div>

		<div class="show-more-wrap">
			<span class="Brief_Description-t"> Brief Description: ${Brief_Description}; </span>
			 </br>
			<span class="last_Visit-t">${last_Visit}; </span>
			</div>
		</div>
		<div class="card-utility">
			<div class="edit-utility">
   				<img src="img/pen-solid.svg" class="edit-card"  alt="edit-card" width="20px">
			</div>
			<div class="close-utility"   >
   				<img src="img/cancel.svg" class="close-card" alt="close-card" width="20px">
			</div>
		</div>   
	</div>
		`
		let container = document.querySelector('.cards-container')
		container.insertAdjacentHTML('afterbegin', VistCard)
	} catch (e) {
		console.error(e)
	}
}
export default renderCard
