const show = (event) => {
	let item = event.target.closest('.glass-card').id
	let element = JSON.parse(localStorage.getItem(`${item}`))
	console.log(element.Doc)

	let showContainer = document.querySelector('.show-more-container')
	let testmodal = showContainer.querySelector('.show-more-modal')

	let showMoreBodyDentist = `
   
    <div class="show-more-card">
    <div class="card-wrap">
        <h5 class="show-more-title">${element.purpose}</h5>
    </div>

    <div class="card-body">
        <p class="show-more-body">${element.full_name}</p>
        <p class="show-more-body">${element.urgency}</p>
    </div>

    <hr>

    <div class="card-body">
        <p class="show-more-body">Description: </p>
        <p class="show-more-body">${element.Brief_Description}</p>
    </div>

    <div class="card-body">
        <p class="show-more-body">Last Visit: </p>
        <p class="show-more-body">${element.last_Visit}</p>
    </div>

    <div class="show-more-close">
        <img src="img/cancel.svg" class="show-more-cross" alt="" width="30px">
    </div>
    </div>
    
    `
	let showMoreBodyCardiologist = `
   
    <div class="show-more-card">
    <div class="card-wrap">
        <h5 class="show-more-title">${element.purpose}</h5>
    </div>

    <div class="card-body">
        <p class="show-more-body">${element.full_name}</p>
        <p class="show-more-body">${element.urgency}</p>
    </div>
    <hr>
    <div class="card-body">
        <p class="show-more-body">Description:</p>
        <p class="show-more-body">${element.Brief_Description}</p>
    </div>

    <div class="card-body">
        <p class="show-more-body">Blood pressure:</p>
        <p class="show-more-body">${element.Blood_Pressure}</p>
    </div>
          
    <div class="card-body">
        <p class="show-more-body">Heart illnesses: </p>
        <p class="show-more-body">${element.Heart_illnesses}</p>
    </div>

    <div class="card-body">
        <p class="show-more-body">Mass index:</p>
        <p class="show-more-body">${element.Mass_Index}</p>
    </div>

    <div class="card-body">
        <p class="show-more-body">Age:</p>
        <p class="show-more-body">${element.Age}</p>
    </div>

    <div class="show-more-close">
        <img src="img/cancel.svg" class="show-more-cross" alt="" width="30px">
    </div>
    </div>
    
    `
	let showMoreBodyTherapist = `
   
    <div class="show-more-card">
    <div class="card-wrap">
        <h5 class="show-more-title">${element.purpose}</h5>
    </div>
    <div class="card-body">
        <p class="show-more-body">${element.full_name}</p>
        <p class="show-more-body">${element.urgency}</p>
    </div>
    <hr>
    <div class="card-body">
        <p class="show-more-body">Description: </p>
        <p class="show-more-body">${element.Brief_Description}</p>
    </div>
    <div class="card-body">
        <p class="show-more-body">Age: </p>
        <p class="show-more-body">${element.Age}</p>
    </div>
    <div class="show-more-close">
        <img src="img/cancel.svg" class="show-more-cross" alt="" width="30px">
    </div>
    </div>
    
    `

	switch (element.Doc) {
		case 'Dentist':
			testmodal.insertAdjacentHTML('afterbegin', showMoreBodyDentist)
			break
		case 'Cardiologist':
			testmodal.insertAdjacentHTML('afterbegin', showMoreBodyCardiologist)
			break
		case 'Therapist':
			testmodal.insertAdjacentHTML('afterbegin', showMoreBodyTherapist)
	}

	// console.log(testmodal)
	// let showContainer = document.querySelector('.show-more-container')
	// let testmodal = showContainer.querySelector('.show-more-modal')
	// testmodal.insertAdjacentHTML('afterbegin', showMoreBodyDentist)
	// console.log(testmodal)
}

export default show
