class Visit {
	constructor(purpose, purposeDescription, urgency, full_name) {
		this.purpose = purpose
		this.purposeDescription = purposeDescription
		this.urgency = urgency
		this.full_name = full_name
	}

	// 	reder() {
	// 		const renderInputs = `<div class="visit-select-wrap">
	// 		<div><span class="select-title">Visit purpose</span></div>
	// 		<div><input type="text"/> </div></div>
	// 	<div class="visit-select-wrap"><div><span class="select-title">Brief description</span></div>
	// 	 <div><input type="text"/></div></div><div class="visit-select-wrap">
	// 	  <div><span class="select-title">Your name</span></div><div><input type="text"/></div></div>
	//   <div class="visit-select-wrap"><div><span class="select-title">Urgency</span></div>
	// 	  <select>
	// 		  <option value="All">All</option>
	// 		  <option value="High">High</option>
	// 		  <option value="Normal">Normal</option>
	// 		  <option value="Low">Low</option>
	// 	  </select>
	//   </div>`
	// 	}
}

class VisitCardiologist extends Visit {
	constructor(
		normal_Pressure,
		body_Mass_Index,
		heart_problems,
		age,
		purpose,
		purposeDescription,
		urgency,
		full_name
	) {
		super(purpose, purposeDescription, urgency, full_name)
		this.normal_Pressure = normal_Pressure
		this.body_Mass_Index = body_Mass_Index
		this.heart_problems = heart_problems
		this.age = age
	}
}
class VisitDentist extends Visit {
	constructor(last_Visit, purpose, purposeDescription, urgency, full_name) {
		super(purpose, purposeDescription, urgency, full_name)
		this.last_Visit = last_Visit
	}
}
class VisitTherapist extends Visit {
	constructor(age, purpose, purposeDescription, urgency, full_name) {
		super(purpose, purposeDescription, urgency, full_name)
		this.age = age
	}
}
